package oddstring

import (
	"fmt"
	"sync"
)

var ALPHABET string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
var ALPHAPROTO string = "abcdefghijklmnopqrstuvwxyz"
var ALPHAPUNCT string = "!?.,;:"
var ALPHAEXTRA string = "@#$]}[{'\"^&*()-_=+/><"
var ALPHANUMBS string = "1234567890"

type OddstringMode struct {
	AlphaLower bool //Use lower case in the output
	AlphaUpper bool //Use upper case
	AlphaPunct bool //Use punctuation
	AlphaExtra bool //Use extra characters
	AlphaNumbs bool //Character representations of numbers
}

var alphabetPunct string //alphabet for punctuation
var alphabetLower string //lower case alphabet
var alphabetUpper string //upper case alphabet
var alphabetExtra string //Non-punctuation, non-letter characters
var alphabetNumbs string //Character numbers
var alphabetFinal string //The final, after-init alphabet to use

var masterMu sync.Mutex

// Get this show started
func initl(mode OddstringMode) {

	alphabetPunct = "!,?'."
	alphabetLower = "abcdefghijklmnopqrstuvwxyz"
	alphabetUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	//alphabetExtra = "@#$5678()_+=-/><"
	alphabetExtra = "abcdefghijklmnopqrd"

	masterMu.Lock()
	alphabetFinal = ""

	if mode.AlphaLower {
		alphabetFinal = alphabetFinal + alphabetLower
	}
	if mode.AlphaUpper {
		alphabetFinal = alphabetFinal + alphabetUpper
	}
	if mode.AlphaPunct {
		alphabetFinal = alphabetFinal + alphabetPunct
	}
	if mode.AlphaExtra {
		alphabetFinal = alphabetFinal + alphabetExtra
	}
	if mode.AlphaNumbs {
		alphabetFinal = alphabetFinal + alphabetNumbs
	}

	if alphabetFinal == "" {
		panic("No mode for alphabet use was selected.")
	}

	fmt.Println(len(alphabetFinal))
	masterMu.Unlock()

}
