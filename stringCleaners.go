package oddstring

import "regexp"

func RemoveNonAlpha(input string) string {
	reg := regexp.MustCompile(`[^a-zA-Z0-9\s]+`)
	return reg.ReplaceAllString(input, "")
}
