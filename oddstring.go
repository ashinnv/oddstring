package oddstring

import (
	//"fmt"
	"fmt"
	"math/rand"
	"strings"
	"sync"
)

// RandstringChan() manages a cache of random strings and pipes them down the outstr chan. Should be faster than the simpler modes
func RandStringChanFull(leng int, mode OddstringMode, outstr chan string, creatorRoutines int, alphabetMode OddstringMode) {

	fmt.Println("Mode:", mode)
	//go initl(mode)
	initl(mode)

	masterMu.Lock()
	fmt.Println("af:", alphabetFinal)
	chars := genAlpha(alphabetMode)

	fmt.Println("Chars:", chars)
	masterMu.Unlock()

	for i := 0; i < creatorRoutines; i++ {

		go func() {
			for {
				tmpStr := ""
				for n := 0; n < leng; n++ {
					r := rand.Intn(len(chars))

					//fmt.Println("R:", r)

					tmpChar := chars[r]
					tmpStr = tmpStr + string(tmpChar)
				}
				//fmt.Println("Len:", len(tmpStr))
				outstr <- tmpStr
			}
		}()

	}

	stopper := make(chan bool)
	_ = <-stopper

}

func RandStringChanSimple(leng int, outstr chan string, routines int, echoMode bool, alphabetMode OddstringMode) {

	var wg sync.WaitGroup
	wg.Add(1)

	chars := genAlpha(alphabetMode)

	if echoMode {
		fmt.Println("af:", alphabetFinal)
		fmt.Println("Chars:", chars)
	}
	for i := 0; i < routines; i++ {
		if echoMode {
			fmt.Printf("Starting routine: \n\n")
		}

		go func() {
			for {
				tmpStr := ""
				for n := 0; n < leng; n++ {
					r := rand.Intn(len(chars))

					tmpChar := chars[r]
					tmpStr = tmpStr + string(tmpChar)
				}
				//fmt.Println("Len:", len(tmpStr))
				outstr <- tmpStr
			}
		}()

	}

	wg.Wait()
}

func RandStringSingle(leng int, echoMode bool, alphabetMode OddstringMode) string {
	//localAlphabet := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+=-,.<>/?"
	chars := genAlpha(alphabetMode)

	if echoMode {
		fmt.Println("af:", alphabetFinal)
		fmt.Println("Chars:", chars)
	}

	tmpStr := ""
	for n := 0; n < leng; n++ {
		r := rand.Intn(len(chars))

		tmpChar := chars[r]
		tmpStr = tmpStr + string(tmpChar)
	}

	return tmpStr
}

func RandStringSingleFull(leng int, verboseMode bool, alphabetMode OddstringMode) string {
	//localAlphabet := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+=-,.<>/?"
	chars := genAlpha(alphabetMode)
	if verboseMode {
		fmt.Println("af:", alphabetFinal)
		fmt.Println("Chars:", chars)
	}

	tmpStr := ""
	for n := 0; n < leng; n++ {
		r := rand.Intn(len(chars))

		tmpChar := chars[r]
		tmpStr = tmpStr + string(tmpChar)
	}

	return tmpStr
}

func ByteStringSimple(count int, aMode OddstringMode) []byte {
	return []byte(RandStringSingleFull(count, false, aMode))
}

// Generate the alphabet that we use for the random string
func genAlpha(mode OddstringMode) []string {
	var lower string = ""
	var upper string = ""
	var numbs string = ""
	var extra string = ""
	var punct string = ""

	if mode.AlphaUpper {
		upper = strings.ToUpper(ALPHAPROTO)
	}

	if mode.AlphaLower {
		lower = ALPHAPROTO
	}

	if mode.AlphaExtra {
		extra = ALPHAEXTRA
	}

	if mode.AlphaPunct {
		punct = ALPHAPUNCT
	}

	if mode.AlphaNumbs {
		numbs = ALPHANUMBS
	}

	return strings.Split(upper+lower+extra+punct+numbs, "")
}
