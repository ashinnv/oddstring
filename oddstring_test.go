package oddstring

import (
	"fmt"
	"testing"
)

var mode OddstringMode = OddstringMode{
	true,
	true,
	true,
	true,
	true,
}

func TestFull(t *testing.T) {

	for i := 0; i < 100000000; i++ {
		if i%1000 == 0 {
			fmt.Println(i, "====", RandStringSingleFull(125, false, mode))
		}
	}

}

func TestSimple(t *testing.T) {
	for i := 0; i < 100000000; i++ {
		if i%1000 == 0 {
			fmt.Println(i, "====", RandStringSingle(125, false, mode))
		}
	}
}
